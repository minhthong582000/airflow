#!/bin/bash

export AIRFLOW_NAMESPACE="airflow-cluster"

kubectl create secret generic \
  airflow-ssh-git-secret \
  --from-file=id_rsa=$HOME/.ssh/id_rsa \
  --namespace $AIRFLOW_NAMESPACE

kubectl apply -R -f ./yamls -n $AIRFLOW_NAMESPACE
