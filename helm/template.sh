## set the release-name & namespace
export AIRFLOW_NAME="airflow-cluster"
export AIRFLOW_NAMESPACE="airflow-cluster"

## install using helm 3
helm template \
  $AIRFLOW_NAME \
  airflow-stable/airflow \
  --namespace $AIRFLOW_NAMESPACE \
  --version "8.X.X" \
  --values ./values.yaml \
  --dry-run \
  --debug \
  --output-dir ./yamls \
  
## wait until the above command returns (may take a while)