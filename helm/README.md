# KubernetesExutor

Sử dụng KubernetesExutor

- disable redis
- disable flower
- disable worker

## How to run ?

Helm template lừa đảo :(

```
kubectl apply -f template.yaml
```

## Config pods

Có 2 cách để worker pods nhận dags file:

- git-sync enable lên, sửa config, thêm git repo...
- volume mount, trước đó phải persistent dags folder và tạo pods template với nội dung như sau:

### Volume mount

- Phải persistent dags folder. (1)

- Tạo pods template. (2)

- Mount (1) và (2) vào Deployment (lưu ý: của web và scheduler, nhớ kiểm tra lại).

(2) có nội dung như sau:

```
      apiVersion: v1
      kind: Pod
      metadata:
        name: dummy-name
        namespace: airflow-cluster
      spec:
        restartPolicy: Never
        serviceAccountName: airflow-cluster
        nodeSelector:
          {}
        affinity:
          {}
        tolerations:
          []
        securityContext:
          fsGroup: 0
        containers:
          - name: base
            image: apache/airflow:2.1.2-python3.8
            imagePullPolicy: IfNotPresent
            securityContext:
              runAsUser: 50000
              runAsGroup: 0
            envFrom:
              - secretRef:
                  name: airflow-cluster-config-envs
            env:
              ## enable the `/entrypoint` db connection check
              - name: CONNECTION_CHECK_MAX_COUNT
                value: "20"
              ## KubernetesExecutor Pods use LocalExecutor internally
              - name: AIRFLOW__CORE__EXECUTOR
                value: LocalExecutor
              - name: DATABASE_PASSWORD
                valueFrom:
                  secretKeyRef:
                    name: airflow-cluster-postgresql
                    key: postgresql-password
              - name: REDIS_PASSWORD
                value: ""
              - name: CONNECTION_CHECK_MAX_COUNT
                value: "0"
            ports: []
            command: []
            args: []
            volumeMounts:
            - name: dags-data
              mountPath: /opt/airflow/dags
              subPath:
              readOnly: false
        volumes:
        - name: dags-data
          persistentVolumeClaim:
            claimName: airflow-cluster-dags
```

## Add Config

Sau đó thêm config:

```
AIRFLOW__KUBERNETES__POD_TEMPLATE_FILE: "/opt/airflow/pod_templates/pod_template.yaml"
AIRFLOW__KUBERNETES__NAMESPACE: "airflow-cluster"
```

## Notes

Flow:

- WebUI -> airflow command inside Web Pod -> kubernetes -> pods -> airflow command to run .py file (Do đó phải mount pods template (2) vào web)

- Scheduler -> airflow command inside Web Pod -> kubernetes -> pods -> airflow command to run .py file

- Để test 1 pods template: exec vào web hoặc scheduler sau đó chạy:

```
airflow kubernetes generate-dag-yaml <dag_id> <running_date>
```

- Trong Pod-template (2) spec.containers[0] phải là base để airflow override
