---
# Source: airflow/templates/pgbouncer/pgbouncer-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: airflow-cluster-pgbouncer
  labels:
    app: airflow
    component: pgbouncer
    chart: airflow-8.5.2
    release: airflow-cluster
    heritage: Helm
spec:
  replicas: 1
  strategy:
    rollingUpdate:
      ## multiple pgbouncer pods can safely run concurrently
      maxSurge: 1
      maxUnavailable: 0
  selector:
    matchLabels:
      app: airflow
      component: pgbouncer
      release: airflow-cluster
  template:
    metadata:
      annotations:
        checksum/secret-config-envs: d275eb17d643320533354d55f1856b4e1ce0e7c11de0fd86cfebd38602563311
        checksum/secret-pgbouncer: 8b1e7a3ce234f5f7d1ad49823aa3e434761e760a1c2964a825075d1f66b10b75
        cluster-autoscaler.kubernetes.io/safe-to-evict: "true"
      labels:
        app: airflow
        component: pgbouncer
        release: airflow-cluster
    spec:
      restartPolicy: Always
      nodeSelector:
        {}
      affinity:
        {}
      tolerations:
        []
      securityContext:
        fsGroup: 0
      terminationGracePeriodSeconds: 120
      serviceAccountName: airflow-cluster
      containers:
        - name: pgbouncer
          image: ghcr.io/airflow-helm/pgbouncer:1.15.0-patch.0
          imagePullPolicy: IfNotPresent
          securityContext:
            runAsUser: 1001
            runAsGroup: 1001
          resources:
            {}
          envFrom:            
            - secretRef:
                name: airflow-cluster-config-envs
          env:            
            - name: DATABASE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: airflow-cluster-postgresql
                  key: postgresql-password
            - name: REDIS_PASSWORD
              value: ""
            - name: CONNECTION_CHECK_MAX_COUNT
              value: "0"
          ports:
            - name: pgbouncer
              containerPort: 6432
              protocol: TCP
          command:
            - "/usr/bin/dumb-init"
            ## rewrite SIGTERM as SIGINT, so pgbouncer does a safe shutdown
            - "--rewrite=15:2"
            - "--"
          args:
            - "/bin/sh"
            - "-c"
            ## we generate users.txt on startup, because DATABASE_PASSWORD is defined from a Secret,
            ## and we want to pickup the new values on container restart (possibly due to livenessProbe failure)
            - |-
              /home/pgbouncer/config/gen_auth_file.sh && \
              exec pgbouncer /home/pgbouncer/config/pgbouncer.ini
          livenessProbe:
            initialDelaySeconds: 5
            periodSeconds: 30
            timeoutSeconds: 15
            failureThreshold: 3
            exec:
              command:
                - "/bin/sh"
                - "-c"
                ## this check is intended to fail when the DATABASE_PASSWORD secret is updated,
                ## which would cause `gen_auth_file.sh` to run again on container start
                - psql $(eval $DATABASE_PSQL_CMD) --tuples-only --command="SELECT 1;" | grep -q "1"
          readinessProbe:
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 5
            failureThreshold: 3
            tcpSocket:
              port: 6432
          volumeMounts:
            - name: pgbouncer-config
              mountPath: /home/pgbouncer/config
              readOnly: true
            - name: pgbouncer-certs
              mountPath: /home/pgbouncer/certs
              readOnly: true
      volumes:
        - name: pgbouncer-config
          secret:
            secretName: airflow-cluster-pgbouncer
            items:
              - key: gen_auth_file.sh
                path: gen_auth_file.sh
                mode: 0755
              - key: pgbouncer.ini
                path: pgbouncer.ini
        - name: pgbouncer-certs
          projected:
            sources:
              ## CLIENT TLS FILES (CHART GENERATED)
              - secret:
                  name: airflow-cluster-pgbouncer-certs
                  items:
                    - key: client.key
                      path: client.key
                    - key: client.crt
                      path: client.crt
