#!/bin/bash

export AIRFLOW_NAMESPACE="airflow-cluster"

kubectl delete -R -f ./yamls -n $AIRFLOW_NAMESPACE

kubectl delete pvc -n $AIRFLOW_NAMESPACE data-airflow-cluster-postgresql-0
